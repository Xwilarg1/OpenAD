#!/usr/bin/env python3

import asyncio
import _thread
import pika
import unittest
from threading import Thread
from message import MessageReceiver
from event import Event_ts

class Test(unittest.IsolatedAsyncioTestCase):

    async def test_rabbitmq(self):
        e = Event_ts()

        def callback(msg):
            self.assertEqual(msg, "Hello World!")
            e.set()

        def on_ready():
            connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
            channel = connection.channel()

            channel.queue_declare(queue='hello')

            channel.basic_publish(exchange='', routing_key='hello', body='Hello World!')

        def start_receive():
            MessageReceiver(callback, on_ready)

        Thread(target=start_receive).start()

        await e.wait()

if __name__ == '__main__':
    unittest.main()