# From https://stackoverflow.com/questions/33000200/asyncio-wait-for-event-from-other-thread

import asyncio

class Event_ts(asyncio.Event):
    def set(self):
        self._loop.call_soon_threadsafe(super().set)