#!/usr/bin/env python3

from os import path
import json
from bot import Bot

def main():
    if path.exists("config.json"):
        with open('config.json', 'r') as f:
            data = f.read()
            j = json.loads(data)
            if "botToken" in j:
                Bot(j["botToken"])
            else:
                print("Missing key \"botToken\"")
    else:
        print("Missing file config.json")

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupéted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
