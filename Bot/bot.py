import discord
from threading import Thread
from message import MessageReceiver

class Bot():
    def __init__(self, token):

        client = discord.Client()

        @client.event
        async def on_ready():
            print('Bot logged as {0.user}'.format(client))

        @client.event
        async def on_message(message):
            if message.author == client.user:
                return

            if message.content.startswith('$hello'):
                await message.channel.send('Hello!')

        def callback(msg):
            pass

        def on_ready():
            pass

        def start_receive():
            MessageReceiver(callback, on_ready)

        Thread(target=start_receive).start()
        client.run(token)
