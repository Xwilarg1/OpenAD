## Installation
 - First download and launch [https://www.rabbitmq.com/download.html](rabbitmq)
 - Install the dependencies: `pip install -r requirements.txt`
 - Create a `config.json` file which contains a field "botToken" with the token of your bot

## How to use it
Start the bot: `python3 main.py`\
Start the tests: `python3 test.py`