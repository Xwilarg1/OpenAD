import pika

class MessageReceiver():
    def __init__(self, callback, on_ready):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.queue_declare(queue='hello')

        def callback_internal(ch, method, properties, body):
            callback(body.decode("utf-8"))

        channel.basic_consume(queue='hello', on_message_callback=callback_internal, auto_ack=True)
        on_ready()
        print("Receiver is ready")
        channel.start_consuming()