# OpenAD
## An open source alternative to other alt detection bots, but most similar to AltDentifier.
### Focus is on stability and performance, with more per server customization. The frontend is running VueJS (w/ Vuetify) and the backend is running Flask. The bot is using Discord.py. Redis is used for none permenant storage and messaging while MongoDB handles the permenant database.

___

## How to run the system:
### TBD but will likely be run with docker-compose.

___

## Portions of this project use the Authlib and Loginpass libraries

Authlib, Loginpass:
Copyright (c) 2017, Hsiaoming Yang
All rights reserved.




